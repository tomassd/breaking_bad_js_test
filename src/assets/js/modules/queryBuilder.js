class QueryBuilder {

    static getInstance() {
        return new QueryBuilder()
    }

    /**
     * Get current param from query search string
     * @param param
     * @returns {string|null}
     */
    static getCurrentParam(param) {
        let search = new URLSearchParams(window.location.search)
        return search.get(param) || null
    }

    /**
     * Set/Modify query search string by key and value
     * @param query
     */
    set queryParam(query) {
        let searchParams = new URLSearchParams(window.location.search)
        searchParams.set(query.key, query.value)
        history.pushState(null, null, '?' + searchParams.toString())
    }
}
export default QueryBuilder