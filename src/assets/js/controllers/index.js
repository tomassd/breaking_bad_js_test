import Logo from '../../images/breaking_bad_logo.png';
import Filters from '../modules/Filters/filters';
import FilterTypes from '../modules/Filters/types';

class Index {

    static getInstance() {
        return new Index();
    }

    constructor() {
        this.allFilterTypes = FilterTypes.getTypes().all;
        this.logo = document.querySelector('.logo');
        this.createLogo();
        this.init();
    }

    init() {
        Filters.getInstance(this.allFilterTypes);
    }

    createLogo() {
        let image = new Image();
        image.src = Logo;
        this.logo.appendChild(image);
    }

}

export default Index;