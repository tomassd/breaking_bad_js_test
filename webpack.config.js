const path = require('path')
const HtmlPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
    mode: 'development',
    entry: './src/assets/js/app.js',
    output: {
        path: path.resolve(__dirname, './dist/'),
        filename: 'build.js?[chunkhash]'
    },
    devServer: {
        port: 3000
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                exclude: /node-modules/,
                use: [
                    "style-loader",
                    "css-loader",
                ],
            },
            {
                test: /\.scss$/i,
                exclude: /node-modules/,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader",
                ],
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[chunkhash]',
                },
            },
        ]
    },
    plugins: [
        new HtmlPlugin({
            template: "./src/index.html"
        }),
        new CleanWebpackPlugin()
    ],
}