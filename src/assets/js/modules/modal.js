class Modal {

    static getInstance() {
        return new Modal()
    }

    constructor() {
        this.$main = document.querySelector('main')
    }

    openModal(html) {
        let modal = document.createElement('div')
        modal.classList.add('modal')
        modal.classList.add('active')
        let content = document.createElement('div')
        content.classList.add('modal-content')
        content.innerHTML = html
        modal.innerHTML = content.innerHTML
        modal.onclick = () => {
            modal.classList.remove('active')
        }
        this.$main.after(modal)
    }

}
export default Modal