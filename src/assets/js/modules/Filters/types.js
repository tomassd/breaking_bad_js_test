class Types {

    static getTypes() {
        return new Types()
    }

    constructor() {
        this.default = 'default'
        this.byNameAsc = 'byNameAsc'
        this.byNameDesc = 'byNameDesc'
        this.byAppearanceAsc = 'byAppearanceAsc'
        this.byAppearanceDesc = 'byAppearanceDesc'
        let ASC = '&#8593'
        let DESC = '&#8595'
        this.all = {
            'default' : 'Character ID',
            'byNameAsc' : 'Name ' + ASC,
            'byNameDesc' : 'Name ' + DESC,
            'byAppearanceAsc' : 'Appearance ' + ASC,
            'byAppearanceDesc' : 'Appearance ' + DESC,
        }
    }
}

export default Types