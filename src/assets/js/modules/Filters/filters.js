import Results from '../results'
import CookiesHandler from '../cookiesHandler'

class Filters extends Results {

    /**
     *  Creates filters
     * @param filterTypes
     * @returns {Filters}
     */
    static getInstance(filterTypes = {}) {
        return new Filters(filterTypes)
    }

    constructor(filterTypes) {
        super()
        this.filterTypes = filterTypes
        this.$filters = document.querySelector('.filters')
        this.cookies = CookiesHandler.getInstance()
        this.buildFilters()
        this.initOnChangeEvent()
    }

    buildFilters() {
        let filters = document.createElement('select')
        let label = document.createElement('label')
        let orderType = this.getOrderBy()
        label.innerHTML = 'Order by: '
        filters.classList.add('dropdown')

        for (const [key, value] of Object.entries(this.filterTypes)) {
                let option = document.createElement('option')
                option.value = key

                if (key === orderType) {
                    option.selected = true
                }
                option.id = key + '-id'
                option.innerHTML = value
                filters.appendChild(option)
        }
        this.$filters.appendChild(label)
        this.$filters.appendChild(filters)

    }

    initOnChangeEvent() {
        const select = document.querySelector('.filters select')
        select.addEventListener('change', (e) => {
            let orderBy =  e.target.value
            this.cookies.set = {
                key: 'orderBy',
                value: orderBy,
            }
            Results.getInstance()
        })
    }
}

export default Filters