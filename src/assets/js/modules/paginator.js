class Paginator {

    constructor(items, currentPage, perPageItems) {
        this.items = items
        this.page = currentPage || 1
        this.perPage = perPageItems || 4
        this.offset = (this.page - 1) * this.perPage
    }

    paginate() {
        let paginatedItems = this.items.slice(this.offset).slice(0, this.perPage),
            totalPages = Math.ceil(this.items.length / this.perPage);

        return {
            page: this.page,
            per_page: this.perPage,
            pre_page: this.page - 1 ? this.page - 1 : null,
            next_page: (totalPages > this.page) ? this.page + 1 : null,
            total: this.items.length,
            total_pages: totalPages,
            data: paginatedItems,
        };
    }
}

export default Paginator;