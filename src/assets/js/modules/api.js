class Api {

    static getInstance() {
        return new Api()
    }

    constructor() {
        this.apiUrl = 'https://www.breakingbadapi.com/api' //todo move to .env
        this.apiType= 'characters'

        this.apiData = {
            url: this.apiUrl,
            type: this.apiType,
        }
    }

    /**
     * fetchCharacters
     * @returns {Promise<*>}
     */
    async fetchCharacters() {
        const { url, type } = this.apiData;
        const apiUrl = `${url}/${type}`

        return await fetch(apiUrl)
            .then(response => response.json());
    }
}

export default Api