import Cookies from 'js-cookie'
class CookiesHandler {

    static getInstance() {
        return new CookiesHandler();
    }

    /**
     * Get cookie by name
     * @param param
     * @returns {String|Number|null}
     */
    static getCurrentCookie(param) {
        return Cookies.get(param) || null
    }

    /**
     * Setting browser cookie by key and value
     * @param options
     */
    set set(options) {
        Cookies.set(options.key, options.value)
    }
}

export default CookiesHandler