import Api from './api'
import Paginator from './paginator'
import FilterTypes from './Filters/types'
import QueryBuilder from './queryBuilder'
import CookiesHandler from './cookiesHandler'
import Modal from './modal'

class Results {

    static getInstance() {
        return new Results()
    }

    constructor(currPage = 1) {
        this.webApi = Api.getInstance()
        this.queryBuilder = QueryBuilder.getInstance()
        this.modal = Modal.getInstance()
        this.$root = document.querySelector('.root')
        this.loader = document.querySelector('.loader')
        this.characters = {}
        this.totalPages = 0
        this.limit = 4
        this.setCurrentPage(currPage)
        this.init()
    }

    init() {
        this.destroyScrollEvent()
        this.isLoading()
        this.resetResults()
        this.getCharacters()
            .then(() => {
                this.handleResults()
                this.addScrollEvent()
            })

    }

    getCurrentPage() {
        return QueryBuilder.getCurrentParam('page') || 1
    }

    setCurrentPage(page) {
        this.queryBuilder.queryParam = {
            key: 'page',
            value: page,
        }
    }

    addScrollEvent() {
        window.addEventListener('scroll', () => {
            this.handleScrollEvent()
        }, {
            passive: true,
        })
    }
    destroyScrollEvent() {
        window.removeEventListener('scroll', () => {
            this.handleScrollEvent()
        }, false)
    }

    handleScrollEvent() {
        // event on scroll
        const { scrollTop, scrollHeight, clientHeight } = document.documentElement
        let currentPage = this.getCurrentPage()

        if (!this.characters.length) {
            return
        }

        if (currentPage < this.totalPages && scrollTop + clientHeight >= scrollHeight) {
            this.isLoading()
            currentPage++
            this.handleResults(currentPage)
        }
    }

    /**
     * Get characters list rom API
     * @returns {Promise<void>}
     */
    async getCharacters() {
        this.characters = await this.webApi.fetchCharacters()
    }

    resetResults() {
        this.$root.innerHTML = ''
    }

    /**
     * Get ordering from cookie
     * @returns {String|Number}
     */
    getOrderBy() {
        return CookiesHandler.getCurrentCookie('orderBy') || 'default'
    }

    setCurrentPageCookies(page) {
        this.setCurrentPage(page)
    }

    handleResults(page = 1) {
        this.setCurrentPageCookies(page)
        this.orderResults()
        const paginator = new Paginator(this.characters, page, this.limit)
        const { data, total_pages } = paginator.paginate()
        this.totalPages = total_pages
        this.buildHtml(data)
        this.isLoading(false)
    }

    orderResults() {
        let orderType = this.getOrderBy()
        let filterTypes = new FilterTypes()

        switch (orderType) {
            case filterTypes.default: {
                this.characters.sort((a, b) => a.char_id - b.char_id)
                break
            }
            case filterTypes.byNameDesc: {
                this.characters.sort((a, b) => a.name.localeCompare(b.name))
                break
            }
            case filterTypes.byNameAsc: {
                this.characters.sort((a, b) => b.name.localeCompare(a.name))
                break
            }
            case filterTypes.byAppearanceDesc: {
                this.characters.sort((a, b) => parseInt(b.appearance.length) - parseInt(a.appearance.length))
                break
            }
            case filterTypes.byAppearanceAsc: {
                this.characters.sort((a, b) => parseInt(a.appearance.length) - parseInt(b.appearance.length))
                break
            }
        }
    }

    /**
     * Create results HTML
     * @param data
     */
    buildHtml(data) {
        let row = document.createElement('div')
        row.classList.add('row')
        data.forEach(character => {
            let column = document.createElement('div')
            column.className = 'col-sm-6 col-lg-3'
            let characterItem = this.buildCharacterItem(character)
            let characterFullItem = this.buildFullCharacterItem(character)
            column.innerHTML = characterItem
            row.appendChild(column)
            column.onclick = this.createElementOnClickHandler(characterFullItem)
        })
        this.$root.appendChild(row)
    }

    createElementOnClickHandler(character) {
        return () => {
            this.modal.openModal(character)
        }
    }

    /**
     * HTML for list view
     * @param character
     * @returns {string}
     */
    buildCharacterItem(character) {
        return `
            <div class="item">
              <h2 class="item__name">${character.name}</h2>
              <div style="background-image: url('${character.img}')" class="item__img"></div>
              <div class="item__description">
                <p><span>Nickname:</span>${character.nickname}</p>
                <p><span>Birthday:</span> ${character.birthday}</p>
                <p><span>Status:</span> ${character.status}</p>
                <p><span>Season appearance:</span> ${character.appearance.length}</p>
              </div>
            </div>
        `
    }

    /**
     * HTML for preview
     * @param character
     * @returns {string}
     */
    buildFullCharacterItem(character) {
        return `
            <div class="item">
              <h2 class="item__name">${character.name}</h2>
              <div style="background-image: url('${character.img}')" class="item__img"></div>
              <div class="item__description">
                <p><span>Nickname:</span>${character.nickname}</p>
                <p><span>Birthday:</span> ${character.birthday}</p>
                <p><span>Status:</span> ${character.status}</p>
                <p><span>Portrayed:</span> ${character.portrayed}</p> 
                <p><span>Occupation:</span> ${character.occupation.join(', ')}</p>   
                <p><span>Season appearance:</span> ${character.appearance.length}</p>
              </div>
            </div>
        `
    }

    /**
     * Set loading state
     * @param state
     */
    isLoading(state = true) {
        if (!state) {
            return this.loader.classList.remove('loading')
        }
        return this.loader.classList.add('loading')
    }

}

export default Results